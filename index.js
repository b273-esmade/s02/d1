// OOP
	// way of programming applications where resusable blueprints calles classes are used to create objects

// Benefits
	// minimizes repetition
	// improves scalability
	// improves maintainablity
	// improves security

// alert("Hello")


// CODESHARE
//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//create student one
let studentOneName = 'John';
let studentOneEmail = 'john@mail.com';
let studentOneGrades = [89, 84, 78, 88];

//create student two
let studentTwoName = 'Joe';
let studentTwoEmail = 'joe@mail.com';
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = 'Jane';
let studentThreeEmail = 'jane@mail.com';
let studentThreeGrades = [87, 89, 91, 93];

//create student four
let studentFourName = 'Jessie';
let studentFourEmail = 'jessie@mail.com';
let studentFourGrades = [91, 89, 92, 93];


//actions that students may perform will be lumped together
function login(email){
    console.log(`${email} has logged in`);
}

function logout(email){
    console.log(`${email} has logged out`);
}

function listGrades(grades){
    grades.forEach(grade => {
        console.log(grade);
    })
}

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects


// Use an object literal

// ENCAPSULATION

let studentOne = {
	name: "John",
	email: "john@mail.com",
	grades: [89,84,78,88],
	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grades are ${this.grades}`)
	}
}

console.log(`student name is ${studentOne.name}`)